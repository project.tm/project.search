<?php

namespace Project\Search;

use CIBlockElement,
    CIBlockSection;

class Section {

    static public function getSubSection($ID) {
        static $sections = array();
        if (empty($sections[$ID])) {
            $sections[$ID] = array(
                $ID => $ID
            );
            $rsParentSection = CIBlockSection::GetByID($ID);
            if ($arParentSection = $rsParentSection->GetNext()) {
                $arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'], '>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'], '>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
                $rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter, false, array('ID'));
                while ($arSect = $rsSect->GetNext()) {
                    $sections[$ID][$arSect['ID']] = $arSect['ID'];
                }
            }
        }
        return $sections[$ID];
    }

    static public function isSection($ID, $sectionId) {
        $sections = self::getSubSection($sectionId);
        $db_old_groups = CIBlockElement::GetElementGroups($ID, true, array('ID'));
        while ($ar_group = $db_old_groups->Fetch()) {
            if (in_array($ar_group['ID'], $sections)) {
                return true;
            }
        }
        return false;
    }

}
