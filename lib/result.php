<?php

namespace Project\Search;

use CDBResult,
    CIBlockElement,
    CIBlockSection,
    Project\Core\Utility;

class Result {

    static public function getData($searchFilter, $arParams) {
        $arSort = array();
        if ($bIBlockCatalog && $arParams['HIDE_NOT_AVAILABLE'] == 'L')
            $arSort['CATALOG_AVAILABLE'] = 'desc,nulls';
        if (!isset($arSort['CATALOG_AVAILABLE']) || $arParams["ELEMENT_SORT_FIELD"] != 'CATALOG_AVAILABLE')
            $arSort[$arParams["ELEMENT_SORT_FIELD"]] = $arParams["ELEMENT_SORT_ORDER"];
        if (!isset($arSort['CATALOG_AVAILABLE']) || $arParams["ELEMENT_SORT_FIELD2"] != 'CATALOG_AVAILABLE')
            $arSort[$arParams["ELEMENT_SORT_FIELD2"]] = $arParams["ELEMENT_SORT_ORDER2"];
        $tempalteName = $arParams['PAGER_TEMPLATE'];
        $arResult = Utility::useCache(array(__CLASS__, __FUNCTION__, $searchFilter, $arSort, $tempalteName), function() use($searchFilter, $arSort, $tempalteName, $arParams) {
                    $arFilter = array(
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                        "IBLOCK_LID" => SITE_ID,
                        "IBLOCK_ACTIVE" => "Y",
                        "ACTIVE_DATE" => "Y",
                        "ACTIVE" => "Y",
                        "CHECK_PERMISSIONS" => "Y",
                        "MIN_PERMISSION" => "R",
                        "INCLUDE_SUBSECTIONS" => ($arParams["INCLUDE_SUBSECTIONS"] == 'N' ? 'N' : 'Y'),
                        "CATALOG_SHOP_QUANTITY_1" => 1,
                    );
                    $arSelect = array(
                        "ID",
                        "CATALOG_GROUP_1"
                    );
                    $arNavParams = array(
                        "nPageSize" => $arParams["PAGE_ELEMENT_COUNT"],
                        "bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
                        "bShowAll" => $arParams["PAGER_SHOW_ALL"],
                    );
                    $arNavigation = CDBResult::GetNavParams($arNavParams);
                    if ($arNavigation["PAGEN"] == 0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] > 0)
                        $arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];

                    pre($arSort);
                    $arResult = array();
                    $rsElements = CIBlockElement::GetList($arSort, array_merge($searchFilter, $arFilter), false, $arNavParams, $arSelect);
                    $rsElements->SetUrlTemplates($arParams["DETAIL_URL"]);
                    while ($arItem = $rsElements->Fetch()) {
                        pre($arItem['ID'], $arItem['CATALOG_PRICE_1']);
                    }

//                    pre($arSort, $pagerTempalteName);
                    return $arResult;
                });

        pre($arResult);
    }

}
