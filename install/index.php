<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class project_search extends CModule {

    public $MODULE_ID = 'project.search';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_SEARCH_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_SEARCH_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_SEARCH_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallEvent();
        Loader::includeModule($this->MODULE_ID);
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /*
     * InstallEvent
     */

    public function InstallEvent() {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler('search', 'BeforeIndex', $this->MODULE_ID, '\Project\Search\Event', 'BeforeIndex');
    }

    public function UnInstallEvent() {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler('search', 'BeforeIndex', $this->MODULE_ID, '\Project\Search\Event', 'BeforeIndex');
    }

}
